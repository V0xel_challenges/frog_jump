﻿#include <vector>
#include <algorithm>
#include <unordered_set>

class Solution
{
public:
	struct Option
	{
		Option(const int j, const int s) :
			jump(j),
			stone(s)
		{}
		int jump;
		int stone;

		bool operator==(const Option& p) const
		{
			return jump == p.jump && stone == p.stone;
		}
	};

	class MyHash
	{
	public:

		size_t operator()(const Option& p) const
		{
			return (std::hash<int>()(p.jump)) ^ (std::hash<int>()(p.stone));
		}
	};

	std::unordered_set<Option, MyHash>checkedOptions;

	bool canCross(std::vector<int>& stones)
	{
		if (stones.empty() || stones.size() == 1) //if no stones or 1 then there is a valid path
		{
			return true;
		}
		return canCross(stones, stones.begin());
	}
	bool canCross(std::vector<int>& stones, const std::vector<int>::iterator baseItr,
		const int jump = 1, const int currStone = 0)
	{
		const Option newOption = { jump, currStone };

		if (jump < 1)
		{
			return false;
		}
		if (checkedOptions.find(newOption) != checkedOptions.cend())
		{
			return false;
		}

		auto itr = baseItr;
		const int jStone = currStone + jump;
		itr = std::lower_bound(itr, stones.end() - 1, jStone);
		checkedOptions.emplace(newOption);
		if (*itr == jStone) //if stone exists
		{
			if (*itr == stones.back())
			{
				return true;	//There is a valid path
			}
			return  canCross(stones, itr, jump - 1, jStone) || canCross(stones, itr, jump, jStone)
				|| canCross(stones, itr, jump + 1, jStone); // Check deeper
		}
		else
		{

			return false;
		}
	}
};

